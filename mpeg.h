#ifndef MPEG_H
#define MPEG_H

#include "mpegMacros.h"
#include "bitstream.h"
#include "huffman.h"
#include "vlclookup.h"
#include "idct.h"
#include <vector>
#include <list>
#include <mutex>
#include <stdlib.h>
#include <opencv2/opencv.hpp>

/* Constant Values */
const static float aspRatioTable[16] = {
	-1.0, 1.0, 0.6735, 0.7031,
	0.7615, 0.8055, 0.8437, 0.8935,
	0.9375, 0.9815, 1.0255, 1.0695,
	1.1250, 1.1575, 1.2015, -1.0
};
const static float picRateTable[16] = {
	-1.0, 23.976, 24.0, 25.0, 29.97,
	30.0, 50.0, 59.94, 60.0
};
const static uint16_t intraQMatrixDefault[64] = {
	8, 16, 19, 22, 26, 27, 29, 34,
	16, 16, 22, 24, 27, 29, 34, 37,
	19, 22, 26, 27, 29, 34, 34, 38,
	22, 22, 26, 27, 29, 34, 37, 40,
	22, 26, 27, 29, 32, 35, 40, 48,
	26, 27, 29, 32, 35, 40, 48, 58,
	26, 27, 29, 34, 38, 46, 56, 69,
	27, 29, 35, 38, 46, 56, 69, 83
};

const static int ZigZagArray[64] = {
	0,   1,   5,  6,   14,  15,  27,  28,
	2,   4,   7,  13,  16,  26,  29,  42,
	3,   8,  12,  17,  25,  30,  41,  43,
	9,   11, 18,  24,  31,  40,  44,  53,
	10,  19, 23,  32,  39,  45,  52,  54,
	20,  22, 33,  38,  46,  51,  55,  60,
	21,  34, 37,  47,  50,  56,  59,  61,
	35,  36, 48,  49,  57,  58,  62,  63
};

const static int blockChannel[] = {0, 0, 0, 0, 1, 2};

static Tables vlcLookup;
static IDCTsolver idctSolver;

/* MPEG frame for use in opencv */
struct playFrame {
	// Constructor
	playFrame(int f, int width, int height, uint8_t t) {
		frame.create(height, width, CV_8UC3);
		frameNo = f;
		type = t;
	}
	uint8_t type;
	uint32_t frameNo;
	cv::Mat frame;
};

/* MPEG Layer Structures */
struct Block {
	int dct_zz[64];
	int dct_dc_past[3];
};
struct MB {
	int past_intra_address;
	int macroblock_address;
	uint16_t mb_row, mb_col;
	uint8_t  macroblock_type;
	uint8_t  quantizer_scale;
	int recon_right_for, recon_down_for;
	int recon_right_for_prev, recon_down_for_prev;
	int recon_right_back, recon_down_back;
	int recon_right_back_prev, recon_down_back_prev;
	int motion_horizontal_forward_code,
	     motion_horizontal_backward_code,
	     motion_vertical_forward_code,
	     motion_vertical_backward_code,
	     motion_horizontal_forward_r,
	     motion_horizontal_backward_r,
	     motion_vertical_forward_r,
	     motion_vertical_backward_r;
	bool pattern_code[6];
	int reconstruct[64*6];
};

struct Slice {
	uint8_t slice_vertical_position;
};

struct Picture {
	uint16_t temporal_reference;
	uint8_t  picture_coding_type;

	bool full_pel_forward_vector;
	uint8_t forward_r_size;
	uint8_t forward_f;

	bool full_pel_backward_vector;
	uint8_t backward_r_size;
	uint8_t backward_f;

	uint32_t forwardOffset, backwardOffset;
	uint32_t currentFrameOffset;

	std::vector<short int> frameBuffer;
};

struct GOP {
	uint32_t time_code;
};

class mpegSequence {
public:
/* Constructor */
	mpegSequence(const char *filename);

/* Parsing variables */
	uint32_t nextStartCode;
	bitStream inputFile; //Inputfile.

	uint16_t horizontal_size;
	uint16_t vertical_size;
	uint16_t hSizeMB, vSizeMB;
	uint16_t hMBcount, vMBcount;

	float    pel_aspect_ratio;
	float    picture_rate;
	uint8_t  intra_quantizer_matrix[64];
	uint8_t  non_intra_quantizer_matrix[64];

/* Output buffer*/
	std::list<playFrame> playQueue;
	std::mutex playMutex;

/* Layers */
	GOP      currentGOP;
	Picture  currentPicture;
	Slice    currentSlice;
	MB       currentMB;
	Block    currentBlock;

/* Functions */
	// Play functions
	void playSequence(const char *windowName);
	// Parsing functions
	void parseSequenceHeader();
	void parseSequence();
	void parseGOP();
	void parsePicture();
	void parseSlice();
	void parseMB();
	void parseBlock(int blockNo);

	/* Predictive coding functions */
	void reconstructMV();
	void reconstructMBfromMV();
	void reconstructChannelFromMV(
	   short int right_for, short int down_for, short int right_half_for, short int down_half_for,
	   short int right_back, short int down_back, short int right_half_back, short int down_half_back,
	   short int *prevFrame, short int *backFrame, short int *currFrame, int i);

	// Get the next start code
	inline void getStartCode() {
		inputFile.align();
		// Jump to next start code.
		while((nextStartCode = inputFile.readNbits(24, false)) != 1){
			inputFile.readNbits(8);
		}
		nextStartCode = inputFile.readNbits(32);
	}
};

/* Decode DC Coefficients */
inline short int decodeDifferential(uint32_t coded, uint8_t size) {
	//If number is positive.
	if((1 << (size - 1)) & coded) { //From spec
		return coded;
	} else { //Decode as written in standard.
		return (-1 << size) | (coded + 1);
	}
}

/* Sign function: return whether the number is positive or negative.*/
inline int Sign(short int n){
	if(n < 0)return -1;
	else if(n > 0)return 1;
	return 0;
}

#endif
