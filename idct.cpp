#include "idct.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

// http://www.reznik.org/papers/SPIE07_MPEG-C_IDCT.pdf
void IDCTsolver::solve1D(double *input, double *output, bool skipEight) {
	for(int i = 0; i < 8; i++){ //Init input.
		bEven[i] = input[i*(1 + 7*(skipEight))];
	}
	bOdd[0] = bEven[0];
	bOdd[1] = bEven[4];
	bOdd[2] = bEven[2];
	bOdd[3] = bEven[6];
	bOdd[4] = bEven[1] - bEven[7];
	bOdd[7] = bEven[1] + bEven[7];
	bOdd[5] = bEven[3]*sqrt2;
	bOdd[6] = bEven[5]*sqrt2;

	bEven[0] = bOdd[0] + bOdd[1];
	bEven[1] = bOdd[0] - bOdd[1];
	bEven[2] = bOdd[2]*c[0] - bOdd[3]*c[1];
	bEven[3] = bOdd[2]*c[1] + bOdd[3]*c[0];
	bEven[4] = bOdd[4] + bOdd[6];
	bEven[5] = bOdd[7] - bOdd[5];
	bEven[6] = bOdd[4] - bOdd[6];
	bEven[7] = bOdd[7] + bOdd[5];

	bOdd[0] = bEven[0] + bEven[3];
	bOdd[1] = bEven[1] + bEven[2];
	bOdd[2] = bEven[1] - bEven[2];
	bOdd[3] = bEven[0] - bEven[3];
	bOdd[4] = bEven[4]*c[4] - bEven[7]*c[5];
	bOdd[5] = bEven[5]*c[2] - bEven[6]*c[3];
	bOdd[6] = bEven[6]*c[2] + bEven[5]*c[3];
	bOdd[7] = bEven[7]*c[4] + bEven[4]*c[5];

	bEven[0] = bOdd[0] + bOdd[7];
	bEven[1] = bOdd[1] + bOdd[6];
	bEven[2] = bOdd[2] + bOdd[5];
	bEven[3] = bOdd[3] + bOdd[4];
	bEven[4] = bOdd[3] - bOdd[4];
	bEven[5] = bOdd[2] - bOdd[5];
	bEven[6] = bOdd[1] - bOdd[6];
	bEven[7] = bOdd[0] - bOdd[7];
	for(int i = 0; i < 8; i++){// Output
		output[i*(1+7*skipEight)] = bEven[i];
	}
}

/* Solve 8x8 IDCT */
void IDCTsolver::solve2D(int *input, int *output) {
	double buff[64];
	for(int i = 0; i < 64; i++){ //Conversion
		buff[i] = (double)input[i];
	}
	for(int i = 0; i < 64; i+=8) { //Column
		solve1D(buff+i, buff+i, false);
	}
	for(int i = 0; i < 8; i++) { //Row
		solve1D(buff+i, buff+i, true);
	}
	for(int i = 0; i < 64; i++){ //Convert and normalize
		output[i] = (int)(buff[i] * 0.125);
	}
}
