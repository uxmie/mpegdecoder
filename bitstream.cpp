#include "bitstream.h"
#include <stdlib.h>
#include <stdio.h>

/* Constructor */
bitStream::bitStream(const char *fileName) {
	this->open(fileName);
}

/* Destructor */
bitStream::~bitStream(){
	free(buffer);
	fclose(fp);
}

/* Open a file fore the bitstream to read. */
void bitStream::open(const char *fileName) {
	fp = fopen(fileName, "r");
	buffer = (uint8_t *)malloc(streamSize + 4);
	byteOffset = 0;
	byteOffsetInBuffer = 0;
	bitOffset = 7;

	fread(buffer, streamSize + 4, 1, fp);
}


/* read N bits from the bitstream. Decide whether to pop at  */
uint32_t bitStream::readNbits(int nbits, bool pop){
	// Trivial case of 0 bits.
	if(!nbits){
		return 0;
	}

	uint32_t val = 0;
	int remaining = nbits;
	uint32_t newByteOffset = byteOffsetInBuffer;
	uint32_t newBitOffset = bitOffset;

	if(nbits > 32){ // We only read at most 32 bits.
		fprintf(stderr, "ERROR: More than 32 bits read.\n");
		exit(1);
	}

	//printf("(%d %d) ", byteOffsetInBuffer, bitOffset);

	//Read from first byte.
	if(nbits <= bitOffset) {
		val = (buffer[newByteOffset] & ((1<<(bitOffset + 1)) - 1)) >> (bitOffset + 1 - nbits);
		if(pop)
			bitOffset -= nbits;
		return val;
	}

	val = buffer[newByteOffset++] & ((1<<(bitOffset + 1)) - 1);
	remaining -= (bitOffset + 1);
	newBitOffset = 7;

	//Read full bytes.
	while(remaining >= 8) {
		val = (val << 8) | buffer[newByteOffset++];
		remaining -= 8;
	}

	//Read last bits.
	if(remaining) {
		val = (val << remaining)
		 | ((buffer[newByteOffset] >> (8 - remaining)));
		newBitOffset -= remaining;
	}

	//Change position if needed
	if(pop){
		if(newByteOffset >= streamSize){ //Read from file if neccesary
			buffer[0] = buffer[streamSize];
			buffer[1] = buffer[streamSize + 1];
			buffer[2] = buffer[streamSize + 2];
			buffer[3] = buffer[streamSize + 3];
			fread(buffer + 4, streamSize, 1, fp);
			newByteOffset -= streamSize;
		}
		byteOffsetInBuffer = newByteOffset;
		bitOffset = newBitOffset;
	}

	return val;
}

/* Align the bit offset */
void bitStream::align() {
	if(bitOffset != 7){
		byteOffsetInBuffer++;
		bitOffset = 7;
	}
}

/* Debug dump for bit streams. Not used in the program */
void bitStream::dump() {
	printf("Bitstream dump: %d %d\n", byteOffsetInBuffer, bitOffset);
}
