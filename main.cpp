#include <opencv2/opencv.hpp>
#include "mpeg.h"
#include <stdio.h>
#include <stdlib.h>
#include <thread>
#include <chrono>
using namespace std;

/* MAIN FUNCTION STARTS HERE */
int main(int argc, char **argv) {
	if(argc != 2){
		fprintf(stderr, "Usage: mpegDecoder video_file\n");
		exit(1);
	}

	mpegSequence video(argv[1]);
	thread parseThread (&mpegSequence::parseSequence, &video);
	this_thread::sleep_for(chrono::milliseconds(100));
	thread playThread (&mpegSequence::playSequence, &video, argv[1]);
	parseThread.join();
	playThread.join();
}
