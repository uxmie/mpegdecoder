#ifndef MPEG_MACROS_H
#define MPEG_MACROS_H

#define PICTURE_START_CODE 0x00000100
#define SLICE_START_CODE_START 0x00000101
#define SLICE_START_CODE_END 0x000001AF
#define USER_DATA_START_CODE 0x000001B2
#define SEQUENCE_HEADER_CODE 0x000001B3
#define SEQUENCE_ERROR_CODE  0x000001B4
#define EXTENSION_START_CODE 0x000001B5
#define SEQUENCE_END_CODE    0x000001B7
#define GROUP_START_CODE     0x000001B8
#define SYSTEM_START_CODE_START 0x000001B9
#define SYSTEM_START_CODE_END   0x000001FF

#define MB_QUANT 1<<4
#define MB_MOTION_FORWARD 1<<3
#define MB_MOTION_BACKWARD 1<<2
#define MB_PATTERN 1<<1
#define MB_INTRA 1

typedef unsigned char uint8_t;
typedef unsigned short uint16_t;
typedef unsigned int uint32_t;

#endif
