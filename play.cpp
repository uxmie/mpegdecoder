#include "mpeg.h"
#include <iterator>
#include <thread>
#include <chrono>

/* Play a sequence in the playQueue */
void mpegSequence::playSequence(const char *windowName){
	cv::namedWindow(windowName, CV_WINDOW_AUTOSIZE);
	int interval = (int)1000/picture_rate;
	while(1){ //pseudo infinite loop. Ends when all frames are decoded.
		auto i = playQueue.begin();
		auto j = std::next(i, 1);
		if(j != playQueue.end() && j->type == 3){ //Play B frames in front of the previous frame.
			while(1){ //Pseudo infinite loop
				playMutex.lock();
				cv::imshow(windowName, j->frame);
				playMutex.unlock();
				cv::waitKey(interval);

				playMutex.lock();
				j = playQueue.erase(j);
				if(j->type != 3){ // Played all B frames.
					playMutex.unlock();
					break;
				} else if(j == playQueue.end()){ // Since a sequence can't end on a B frame, there must be more, and we wait for them.
					playMutex.unlock();
					std::this_thread::sleep_for(std::chrono::milliseconds(50));
					j = std::next(i, 1);
				} else { // Play more B frames.
					playMutex.unlock();
				}
			}
		}
		playMutex.lock();
		cv::imshow(windowName, i->frame);
		playMutex.unlock();
		cv::waitKey(interval);

		playMutex.lock();
		i = playQueue.erase(i);
		if(i->type == 10){ // Sequence ends. see mpegSequence::parseSequence().
			playMutex.unlock();
			playQueue.pop_front();
			break;
		} else if(i == playQueue.end()){ // Wait for more frames.
			playMutex.unlock();
			std::this_thread::sleep_for(std::chrono::milliseconds(50));
		} else { // Play more frames.
			playMutex.unlock();
		}
	}
}
