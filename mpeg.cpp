#include "mpeg.h"
#include <string.h>
#include <thread>
#include <chrono>

/* Constructor */
mpegSequence::mpegSequence(const char *filename){
	for(int i = 0; i < 64; i++) { //Use the default matrix.
		intra_quantizer_matrix[i] = intraQMatrixDefault[i];
		non_intra_quantizer_matrix[i] = 16;
	}
	inputFile.open(filename);
}

/* Parse an MPEG sequence */
void mpegSequence::parseSequence(){
	nextStartCode = inputFile.readNbits(32);
	while(nextStartCode == SEQUENCE_HEADER_CODE) { //Fail safe.
		parseSequenceHeader();
		while(nextStartCode == GROUP_START_CODE){ //Parse each GOP
			parseGOP();
		}
		printf("Finished parsing GOPs...\n");
	}
	printf("MPEG sequence parsing ended. Please sit back and enjoy the video...\n");
	playMutex.lock();
	playQueue.push_back(playFrame(0, 0, 0, 10));
	playMutex.unlock();
}

/* Parse the header of the MPEG Sequence */
void mpegSequence::parseSequenceHeader(){
	horizontal_size = inputFile.readNbits(12);
	vertical_size = inputFile.readNbits(12);
	pel_aspect_ratio = aspRatioTable[inputFile.readNbits(4)];
	picture_rate = picRateTable[inputFile.readNbits(4)];

	//Skip bit rate information..
	inputFile.readNbits(30);

	if(inputFile.readNbits(1)) {//Read intra_quantizer_table;
		for(int i = 0; i < 64; i++) { //Read table.
			intra_quantizer_matrix[i] = inputFile.readNbits(8);
		}
	}
	if(inputFile.readNbits(1)) {//Read non_intra_quantizer_matrix
		for(int i = 0; i < 64; i++) {//Read table
			non_intra_quantizer_matrix[i] = inputFile.readNbits(8);
		}
	}

	getStartCode();
	uint32_t terminate;
	if(nextStartCode == EXTENSION_START_CODE) { //Skip reserved extension.
		while((terminate = inputFile.readNbits(24, false)) != 1) //Skip
			inputFile.readNbits(8);
		getStartCode();
	}
	if(nextStartCode == USER_DATA_START_CODE) { //Skip User data
		while((terminate = inputFile.readNbits(24, false)) != 1) //Skip.
			inputFile.readNbits(8);
		getStartCode();
	}

	//Add additional info.
	hMBcount = (horizontal_size + 15) >> 4;
	vMBcount = (vertical_size + 15) >> 4;
	hSizeMB  = hMBcount << 4;
	vSizeMB  = vMBcount << 4;

	//Debug dump
	printf("Decoding %d x %d video. Frame rate %f\n", horizontal_size, vertical_size, picture_rate);
}

/* Parse Group of pictures */
void mpegSequence::parseGOP() {
	currentGOP.time_code = inputFile.readNbits(25);
	printf("GOP: current timecode: %02dh %02dm %02ds %02df\n",
				(currentGOP.time_code>>19) & 31,
				(currentGOP.time_code>>13) & 63,
				(currentGOP.time_code>>6) & 63,
				(currentGOP.time_code) & 63
	      );
	inputFile.readNbits(2); //closed_gop and broken_link

	getStartCode();
	if(nextStartCode == EXTENSION_START_CODE) {//Skip reserved extension.
		while(inputFile.readNbits(24, false) != 1){//Skip
			inputFile.readNbits(8);
		}
		getStartCode();
	}
	if(nextStartCode == USER_DATA_START_CODE) {//Skip user data
		while(inputFile.readNbits(24, false) != 1){//Skip
			inputFile.readNbits(8);
		}
		getStartCode();
	}
	while(nextStartCode == PICTURE_START_CODE){//Parse Picture Layer
		parsePicture();
	}
}

/* Parse Picture */
void mpegSequence::parsePicture() {
	currentPicture.temporal_reference = inputFile.readNbits(10);
	currentPicture.picture_coding_type = inputFile.readNbits(3);

	inputFile.readNbits(16); //Ignore cbv

	//Get encoded motion vertores for for P and B frames.
	if(currentPicture.picture_coding_type == 2
	        || currentPicture.picture_coding_type == 3){
		currentPicture.full_pel_forward_vector = inputFile.readNbits(1);

		uint8_t forward_code = inputFile.readNbits(3);
		currentPicture.forward_r_size = forward_code-1;
		currentPicture.forward_f = 1<<currentPicture.forward_r_size;

		if(currentPicture.picture_coding_type == 3) { //Get backwards vectors
			currentPicture.full_pel_backward_vector = inputFile.readNbits(1);

			uint8_t backward_code = inputFile.readNbits(3);
			currentPicture.backward_r_size = backward_code-1;
			currentPicture.backward_f = 1<<currentPicture.backward_r_size;
		} else {  //If the current picture is a P picture, move the offset in the frame buffer.
			currentPicture.forwardOffset = currentPicture.backwardOffset;
			currentPicture.backwardOffset = currentPicture.currentFrameOffset;
		}
	} else if (currentPicture.picture_coding_type == 1) { //Clear the frame buffer if the current frame is type I.
		currentPicture.currentFrameOffset = 0;
		currentPicture.frameBuffer.clear();
		currentPicture.forwardOffset = currentPicture.backwardOffset = 0;
	}
	currentPicture.frameBuffer.resize(
		currentPicture.currentFrameOffset, 0
	);
	currentPicture.frameBuffer.resize(
		currentPicture.currentFrameOffset + hSizeMB*vSizeMB*3, 0
	);
	while(inputFile.readNbits(1, false) == 1){ //Skip user data
		inputFile.readNbits(9);
	}

	printf("\tFrame no %d, coding type: %d\n",
	        currentPicture.temporal_reference,
			currentPicture.picture_coding_type);

	getStartCode();

	//Skipping over reserved data...

	while(nextStartCode >= SLICE_START_CODE_START
	        && nextStartCode <= SLICE_START_CODE_END) { //Parse Slice
		parseSlice();
	}

	//Picture decoded, move to queue.
	playMutex.lock();
	playQueue.push_back(playFrame(currentPicture.temporal_reference,
	                              horizontal_size, vertical_size,
								  currentPicture.picture_coding_type));
	uchar *p;
	short int *buf = &(currentPicture.frameBuffer[currentPicture.currentFrameOffset]);
	float bgrBuf[3];
	// Conver the YCbCr info into BGR, readable by OpenCV
	for(uint16_t i = 0; i < vertical_size; i++) {
		p = playQueue.back().frame.ptr<uchar>(i);
		for(uint16_t j = 0; j < horizontal_size; j++) { //col
			int idxInBuf = i*hSizeMB + j;
			bgrBuf[0] = buf[3*idxInBuf] + 1.772*(buf[3*idxInBuf + 1]-128);
			bgrBuf[1] = buf[3*idxInBuf] - 0.344*(buf[3*idxInBuf + 1]-128) - 0.714*(buf[3*idxInBuf + 2]-128);
			bgrBuf[2] = buf[3*idxInBuf] + 1.402*(buf[3*idxInBuf + 2]-128);
			for(int k = 0; k < 3; k++){ //Clamp colors
				if(bgrBuf[k] < 0)bgrBuf[k] = 0;
				else if(bgrBuf[k] > 255) bgrBuf[k] = 255;
			}
			p[3*j] = bgrBuf[0];
			p[3*j + 1] = bgrBuf[1];
			p[3*j + 2] = bgrBuf[2];
		}
	}
	// If the queue is contains 2 secs worth of pictures, wait for it to play and deflate.
	if(playQueue.size() > picture_rate*2){
		playMutex.unlock();
		std::this_thread::sleep_for(std::chrono::seconds(1));
	} else {
		playMutex.unlock();
	}
	// Move the frame buffer offset if type I or P.
	if(currentPicture.picture_coding_type != 3){
		currentPicture.currentFrameOffset += hSizeMB*vSizeMB*3;
	}
}

/* Parse a slice */
void mpegSequence::parseSlice() {
	currentSlice.slice_vertical_position = nextStartCode & 0xff;
	currentMB.quantizer_scale = inputFile.readNbits(5);

	//Reset block predictors.
	currentMB.past_intra_address = -2;
	currentMB.recon_down_for_prev = currentMB.recon_right_for_prev = 0;
	currentMB.recon_down_back_prev = currentMB.recon_right_back_prev = 0;
	currentMB.macroblock_address
	     = (currentSlice.slice_vertical_position-1)*hMBcount - 1;
	currentBlock.dct_dc_past[0]
		= currentBlock.dct_dc_past[1]
		= currentBlock.dct_dc_past[2] = 1024;

	while(inputFile.readNbits(1) == 1){ //Skip user data.
		inputFile.readNbits(8);
	}
	do { //Parse Macroblock.
		parseMB();
	} while(inputFile.readNbits(23, false));

	getStartCode();
}

/* Parse Macroblock */
void mpegSequence::parseMB() {
	while(inputFile.readNbits(11, false) == 15){ //Skip stuffing
		inputFile.readNbits(11);
	}
	int nescapes = 0;
	while(inputFile.readNbits(11, false) == 8){ //Read escapes.
		inputFile.readNbits(11);
		nescapes++;
	}
	int addrInc = vlcLookup.mbAddrInc.query(inputFile) + nescapes*33;
	currentMB.macroblock_address += addrInc;

	if(addrInc > 1) { //Reconstruct skipped MBs
		if(currentPicture.picture_coding_type == 2) { //Reset motion vectors if type P.
			//currentMB.recon_down_for_prev = currentMB.recon_right_for_prev = 0;
			currentMB.recon_down_for = currentMB.recon_right_for = 0;
		}
		for(int i = 1; i < addrInc; i++){ //Reconstruct each skipped MBs
			int tempAddr = currentMB.macroblock_address - i;
			currentMB.mb_row = tempAddr / hMBcount;
			currentMB.mb_col = tempAddr % hMBcount;
			reconstructMBfromMV();
		}
	}
	currentMB.mb_row = currentMB.macroblock_address / hMBcount;
	currentMB.mb_col = currentMB.macroblock_address % hMBcount;

	int offsetInPic = currentPicture.currentFrameOffset
		+ 3*( ((currentMB.mb_row*hSizeMB)<<4) + (currentMB.mb_col<<4) );
	short int *frameBuf;
		frameBuf = &(currentPicture.frameBuffer[offsetInPic]);

	switch(currentPicture.picture_coding_type){ //Select huffman table to look up.
		case(1):
			currentMB.macroblock_type = vlcLookup.mbTypeI.query(inputFile);
			break;
		case(2):
			currentMB.macroblock_type = vlcLookup.mbTypeP.query(inputFile);
			break;
		case(3):
			currentMB.macroblock_type = vlcLookup.mbTypeB.query(inputFile);
			break;
		case(4):
			currentMB.macroblock_type = vlcLookup.mbTypeDC.query(inputFile);
			break;
		default:
			fputs("Unknown type", stderr);
			exit(1);
	}

	if(currentMB.macroblock_type & MB_QUANT){ //Update quantizer_scale
		currentMB.quantizer_scale = inputFile.readNbits(5);
	}
	if(currentMB.macroblock_type & MB_INTRA){ //Reset motion vectors.
		currentMB.recon_right_for_prev = currentMB.recon_down_for_prev = 0;
		currentMB.recon_right_back_prev = currentMB.recon_down_back_prev = 0;
	}
	if(!(currentMB.macroblock_type & MB_INTRA)
			|| currentMB.macroblock_address - currentMB.past_intra_address > 1){ //Reset dc predictors.
		currentBlock.dct_dc_past[0]
			= currentBlock.dct_dc_past[1]
			= currentBlock.dct_dc_past[2] = 1024;
	}
	if(currentMB.macroblock_type & MB_MOTION_FORWARD){ // Get encoded forward vectors
		currentMB.motion_horizontal_forward_code
		    = vlcLookup.motionVector.query(inputFile);
		if(currentPicture.forward_f != 1
		        && currentMB.motion_horizontal_forward_code != 0) { //motion_horizontal_forward_r, see Standard.
			currentMB.motion_horizontal_forward_r
			    = inputFile.readNbits(currentPicture.forward_r_size);
		}
		currentMB.motion_vertical_forward_code
		    = vlcLookup.motionVector.query(inputFile);
		if(currentPicture.forward_f != 1
		        && currentMB.motion_vertical_forward_code != 0) { //motion_vertical_forward_r, see standard.
			currentMB.motion_vertical_forward_r
			    = inputFile.readNbits(currentPicture.forward_r_size);
		}
	} else if(currentPicture.picture_coding_type == 2){ // No motion vector info for P picture. Reset predictors.
		currentMB.recon_right_for_prev = currentMB.recon_down_for_prev = 0;
		currentMB.recon_down_for = currentMB.recon_right_for = 0;
	} else { // Keep vectors for B pictures.
		currentMB.recon_down_for = currentMB.recon_down_for_prev;
		currentMB.recon_right_for = currentMB.recon_right_for_prev;
	}
	if(currentMB.macroblock_type & MB_MOTION_BACKWARD){ // Get backwards vectors
		currentMB.motion_horizontal_backward_code
		    = vlcLookup.motionVector.query(inputFile);
		if(currentPicture.backward_f != 1
		        && currentMB.motion_horizontal_backward_code != 0) { //motion_horizontal_backward_r
			currentMB.motion_horizontal_backward_r
			    = inputFile.readNbits(currentPicture.backward_r_size);
		}
		currentMB.motion_vertical_backward_code
		    = vlcLookup.motionVector.query(inputFile);
		if(currentPicture.backward_f != 1
		        && currentMB.motion_vertical_backward_code != 0) { //motion_vertical_backward_r
			currentMB.motion_vertical_backward_r
			    = inputFile.readNbits(currentPicture.backward_r_size);
		}
	}
	char cbp;
	if(currentMB.macroblock_type & MB_PATTERN) { // Get MB pattern
		cbp = vlcLookup.mbPattern.query(inputFile);
	} else { //Default pattern code.
		cbp = 0;
	}
	for(int i = 0; i < 6; i++){ // Decode pattern code.
		currentMB.pattern_code[i]
		=  (currentMB.macroblock_type & MB_INTRA) || ((cbp & (1<<(5 - i))) > 0);
	}

	//Reconstruct from other blocks if needed.
	if(!(currentMB.macroblock_type & MB_INTRA)){
		reconstructMV();
		reconstructMBfromMV();
	}

	//decode blocks
	memset(currentMB.reconstruct, 0, 64*6*sizeof(int));
	for(int i = 0; i < 6; i++) { // Parse block.
		if(currentMB.pattern_code[i]) parseBlock(i);
	}
	// Add the decoded blocks to the current frame.
	for(int i = 0; i < 4; i++) { // Y channel
		int MBhOffset = (i & 1), MBvOffset = i >> 1;
		int *buf = &(currentMB.reconstruct[i<<6]);
		frameBuf = &(currentPicture.frameBuffer[
			offsetInPic + 3*(((MBvOffset*hSizeMB)<<3) + (MBhOffset<<3))
		]);
		// Add decoded block
		for(int j = 0,idx = 0; j < 8; j++) {
			for(int k = 0; k < 8; k++, idx++) { //col
				frameBuf[3*(j*hSizeMB + k)] += buf[idx];
			}
		}
	}
	for(int i = 4; i < 6; i++) { // Cb and Cr
		int *buf = &(currentMB.reconstruct[i<<6]);
		frameBuf = &(currentPicture.frameBuffer[offsetInPic]);
		// Run through the 16x16 MB for chrominance block.
		for(int j = 0,idx = 0; j < 16; j+=2) {
			for(int k = 0; k < 16; k+=2, idx++) { //col
				frameBuf[3*(j*hSizeMB + k) + i - 3] += buf[idx];
				frameBuf[3*(j*hSizeMB + (k+1)) + i - 3] += buf[idx];
				frameBuf[3*((j+1)*hSizeMB + k) + i - 3] += buf[idx];
				frameBuf[3*((j+1)*hSizeMB + (k+1)) + i - 3] += buf[idx];
			}
		}
	}

	if(currentMB.macroblock_type & MB_INTRA) { // Update past intracoded MB address.
		currentMB.past_intra_address = currentMB.macroblock_address;
	}
}

/* Parse Block */
void mpegSequence::parseBlock(int blockNo) {
	memset(currentBlock.dct_zz, 0, 64*sizeof(int));
	int *buf = currentMB.reconstruct + (blockNo<<6);
	int zzIdx = 0;
	if(currentMB.macroblock_type & MB_INTRA) { // Decode DC coefficient for intracoded blocks.
		if(blockNo < 4) { // Y
			uint8_t lumDCsize = vlcLookup.dctSizeLum.query(inputFile);
			if(lumDCsize) { // dct_zz[0] != 0
				currentBlock.dct_zz[0] =
					decodeDifferential(inputFile.readNbits(lumDCsize), lumDCsize);
			}
		} else { //Cb, Cr
			uint8_t chrDCsize = vlcLookup.dctSizeChr.query(inputFile);
			if(chrDCsize) { // dct_zz[0] != 0
				currentBlock.dct_zz[0] =
					decodeDifferential(inputFile.readNbits(chrDCsize), chrDCsize);
			}
		}
		buf[0] = currentBlock.dct_dc_past[blockChannel[blockNo]] + currentBlock.dct_zz[0]*8;
		currentBlock.dct_dc_past[blockChannel[blockNo]] = buf[0];
	} else { // Non intra
		int first;
		if(inputFile.readNbits(1, false)){ //Read 10 or 11
			currentBlock.dct_zz[0] = (inputFile.readNbits(2) == 2)?1:-1;
		} else { // Spec
			first = vlcLookup.runLength.query(inputFile);
			if(first == -1){ // Read an escape
				zzIdx = inputFile.readNbits(6);
				int temp = inputFile.readNbits(8);
				if(temp & 0x7f){ // 8-bit level
					currentBlock.dct_zz[zzIdx] = (char)temp;
				} else { // 16-bit level
					currentBlock.dct_zz[zzIdx] = inputFile.readNbits(8);
					if(temp & 0x80){ // Negative level
						currentBlock.dct_zz[zzIdx] -= 256;
					}
				}
			} else { // Normal run-length
				zzIdx = (first >> 8);
				currentBlock.dct_zz[zzIdx] = first & 0xff;
				if(inputFile.readNbits(1)) // Is negative?
					currentBlock.dct_zz[zzIdx] = -currentBlock.dct_zz[zzIdx];
			}
		}
	}
	if(currentPicture.picture_coding_type != 4) { //Not DC picture
		while(int next = vlcLookup.runLength.query(inputFile)) { //Fil dct_zz
			if(next == -1){ // Read escape
				zzIdx += inputFile.readNbits(6) + 1;
				int temp = inputFile.readNbits(8);
				if(temp & 0x7f){ // 8-bit level
					currentBlock.dct_zz[zzIdx] = (char)temp;
				} else { // 16-bit
					currentBlock.dct_zz[zzIdx] = inputFile.readNbits(8);
					if(temp & 0x80){ // Negative?
						currentBlock.dct_zz[zzIdx] -= 256;
					}
				}
			} else { // Normal run level
				zzIdx += (next >> 8) + 1;
				currentBlock.dct_zz[zzIdx] = next & 0xff;
				if(inputFile.readNbits(1)) //Negative?
					currentBlock.dct_zz[zzIdx] = -currentBlock.dct_zz[zzIdx];
			}
		}
	}
	if(currentMB.macroblock_type & MB_INTRA){ //Quantize, dezigzag intra coded block.
		for(int i = 1; i < 64; i++) { // Fill block.
			buf[i] =
				(2*currentBlock.dct_zz[ZigZagArray[i]]
					*currentMB.quantizer_scale*intra_quantizer_matrix[i]) / 16;
			if(!(buf[i] & 1)){ //Oddify
				if(buf[i] > 0)buf[i]--; //>0
				else if(buf[i] < 0)buf[i]++; //<0
			}
			if(buf[i] > 2047)buf[i] = 2047; //Clamp up
			else if(buf[i] < -2048)buf[i] = -2048; //Clamp down
		}
	} else { // Non intra-coded block.
		for(int i = 0; i < 64; i++) { //Fill block.
			short int k = ZigZagArray[i];
			buf[i] =(((2 * currentBlock.dct_zz[k]) + Sign(currentBlock.dct_zz[k]))
			 * currentMB.quantizer_scale * non_intra_quantizer_matrix[i] ) / 16;
			if(!(buf[i] & 1)){ //Oddify
				if(buf[i] > 0)buf[i]--; //>0
				else if(buf[i] < 0)buf[i]++; //<0
			}
			if(buf[i] > 2047)buf[i] = 2047; //Clamp up
			else if(buf[i] < -2048)buf[i] = -2048; //Clamp down
		}
	}
	idctSolver.solve2D(buf, buf);
}

/* reconstruct motion vectors */
void mpegSequence::reconstructMV() {
	short int cpmt_hor_fr, cpmt_ver_fr;
	short int right_little, right_big, down_little, down_big;

	//Reconstruct forward motion vectors
	if(currentMB.macroblock_type & MB_MOTION_FORWARD){ // From spec
		if(currentPicture.forward_f == 1 ||
			   currentMB.motion_horizontal_forward_code == 0) { // From spec
			cpmt_hor_fr = 0;
		} else { // From spec
			cpmt_hor_fr = currentPicture.forward_f - 1 - currentMB.motion_horizontal_forward_r;
		}
		if(currentPicture.forward_f == 1 ||
			   currentMB.motion_vertical_forward_code == 0) { // From spec
			cpmt_ver_fr = 0;
		} else { // From spec
			cpmt_ver_fr = currentPicture.forward_f - 1 - currentMB.motion_vertical_forward_r;
		}

		right_little = currentMB.motion_horizontal_forward_code*currentPicture.forward_f;
		if(!right_little){ // From spec
			right_big = 0;
		} else {
			if(right_little > 0){ // From spec
				right_little -= cpmt_hor_fr;
				right_big = right_little - 32 * currentPicture.forward_f;
			} else { // From spec
				right_little += cpmt_hor_fr;
				right_big = right_little + 32 * currentPicture.forward_f;
			}
		}
		down_little = currentMB.motion_vertical_forward_code*currentPicture.forward_f;
		if(!down_little){ // From spec
			down_big = 0;
		} else { // From spec
			if(down_little > 0){ // From spec
				down_little -= cpmt_ver_fr;
				down_big = down_little - 32 * currentPicture.forward_f;
			} else { // From spec
				down_little += cpmt_ver_fr;
				down_big = down_little + 32 * currentPicture.forward_f;
			}
		}

		short int max = ( 16 * currentPicture.forward_f ) - 1 ;
		short int min = ( -16 * currentPicture.forward_f ) ;
		short int new_vector = currentMB.recon_right_for_prev + right_little ;
		if ( new_vector <= max && new_vector >= min ) // From spec
			currentMB.recon_right_for = currentMB.recon_right_for_prev + right_little ;
		else
			currentMB.recon_right_for = currentMB.recon_right_for_prev + right_big ;
		currentMB.recon_right_for_prev = currentMB.recon_right_for ;
		if ( currentPicture.full_pel_forward_vector ) // From spec
			currentMB.recon_right_for <<= 1;

		new_vector = currentMB.recon_down_for_prev + down_little ;
		if ( new_vector <= max && new_vector >= min ) // From spec
			currentMB.recon_down_for = currentMB.recon_down_for_prev + down_little ;
		else
			currentMB.recon_down_for = currentMB.recon_down_for_prev + down_big ;
		currentMB.recon_down_for_prev = currentMB.recon_down_for ;
		if ( currentPicture.full_pel_forward_vector ) // From spec
			currentMB.recon_down_for <<= 1;
	}
	if(currentMB.macroblock_type & MB_MOTION_BACKWARD) { // Backwards vector, from spec
		if(currentPicture.backward_f == 1 ||
			   currentMB.motion_horizontal_backward_code == 0) { // From spec
			cpmt_hor_fr = 0;
		} else { // From spec
			cpmt_hor_fr = currentPicture.backward_f - 1 - currentMB.motion_horizontal_backward_r;
		}
		if(currentPicture.backward_f == 1 ||
			   currentMB.motion_vertical_backward_code == 0) { // From spec
			cpmt_ver_fr = 0;
		} else { // From spec
			cpmt_ver_fr = currentPicture.backward_f - 1 - currentMB.motion_vertical_backward_r;
		}

		right_little = currentMB.motion_horizontal_backward_code*currentPicture.backward_f;
		if(!right_little){ // From spec
			right_big = 0;
		} else { // From spec
			if(right_little > 0){ // From spec
				right_little -= cpmt_hor_fr;
				right_big = right_little - 32 * currentPicture.backward_f;
			} else { // From spec
				right_little += cpmt_hor_fr;
				right_big = right_little + 32 * currentPicture.backward_f;
			}
		}
		down_little = currentMB.motion_vertical_backward_code*currentPicture.backward_f;
		if(!down_little){ // From spec
			down_big = 0;
		} else { // From spec
			if(down_little > 0){ // From spec
				down_little -= cpmt_ver_fr;
				down_big = down_little - 32 * currentPicture.backward_f;
			} else { // From spec
				down_little += cpmt_ver_fr;
				down_big = down_little + 32 * currentPicture.backward_f;
			}
		}

		short int max = ( 16 * currentPicture.backward_f ) - 1 ;
		short int min = ( -16 * currentPicture.backward_f ) ;
		short int new_vector = currentMB.recon_right_back_prev + right_little ;
		if ( new_vector <= max && new_vector >= min )  // From spec
			currentMB.recon_right_back = currentMB.recon_right_back_prev + right_little ;
		else // From spec
			currentMB.recon_right_back = currentMB.recon_right_back_prev + right_big ;
		currentMB.recon_right_back_prev = currentMB.recon_right_back ;
		if ( currentPicture.full_pel_backward_vector ) // From spec
			currentMB.recon_right_back <<= 1;

		new_vector = currentMB.recon_down_back_prev + down_little ;
		if ( new_vector <= max && new_vector >= min ) // From spec
			currentMB.recon_down_back = currentMB.recon_down_back_prev + down_little ;
		else // From spec
			currentMB.recon_down_back = currentMB.recon_down_back_prev + down_big ;
		currentMB.recon_down_back_prev = currentMB.recon_down_back ;
		if ( currentPicture.full_pel_backward_vector ) // From spec
			currentMB.recon_down_back <<= 1;
	}
}

/* Reconstruct a Macroblock from the motion vectors */
void mpegSequence::reconstructMBfromMV() {

	//Reconstruct motion vectors
	int offsetInPic = 3*( ((currentMB.mb_row*hSizeMB)<<4) + (currentMB.mb_col<<4) );

	short int right_for = currentMB.recon_right_for >> 1;
	short int down_for  = currentMB.recon_down_for >> 1;
	short int right_half_for = currentMB.recon_right_for - 2*right_for;
	short int down_half_for = currentMB.recon_down_for - 2*down_for;
	short int right_back = currentMB.recon_right_back >> 1;
	short int down_back  = currentMB.recon_down_back >> 1;
	short int right_half_back = currentMB.recon_right_back - 2*right_back;
	short int down_half_back = currentMB.recon_down_back - 2*down_back;

	short int *currFrame = &(currentPicture.frameBuffer[
		currentPicture.currentFrameOffset + offsetInPic
	]);
	short int *prevFrame = &(currentPicture.frameBuffer[
		currentPicture.forwardOffset + offsetInPic
	]);
	short int *backFrame = &(currentPicture.frameBuffer[
		currentPicture.backwardOffset + offsetInPic
	]);

	reconstructChannelFromMV(
		right_for, down_for, right_half_for, down_half_for,
		right_back, down_back, right_half_back, down_half_back,
		prevFrame, backFrame, currFrame, 0);
	reconstructChannelFromMV(
		right_for, down_for, right_half_for, down_half_for,
		right_back, down_back, right_half_back, down_half_back,
		prevFrame, backFrame, currFrame, 1);
	reconstructChannelFromMV(
		right_for, down_for, right_half_for, down_half_for,
		right_back, down_back, right_half_back, down_half_back,
		prevFrame, backFrame, currFrame, 2);
}

/* Reconstruct a single channel from the motion vectors */
void mpegSequence::reconstructChannelFromMV(
	   short int right_for, short int down_for, short int right_half_for, short int down_half_for,
	   short int right_back, short int down_back, short int right_half_back, short int down_half_back,
	   short int *prevFrame, short int *backFrame, short int *currFrame, int i) {
	int prevIdx = 3*(down_for*hSizeMB + right_for);
	int backIdx = 3*(down_back*hSizeMB + right_back);
	short int ycbcr, ycbcrb;
	bool forward =(currentMB.macroblock_type & MB_MOTION_FORWARD);
	bool backward = (currentMB.macroblock_type & MB_MOTION_BACKWARD);
	int sampleFor, sampleBack;
	//Run through all pels.
	for(int j = 0; j < 16; j++) {
		for (int k = 0; k < 16; k++) { //col
			ycbcr = 0;
			sampleFor = sampleBack = 0;
			if(currentPicture.picture_coding_type == 2 || forward) { //Forward Vector exists.
				ycbcr = prevFrame[prevIdx + 3*(j*hSizeMB + k) + i];
				sampleFor++;
				if(right_half_for){ //Accumulate half pel
					ycbcr +=prevFrame[prevIdx + 3*(j*hSizeMB + k + 1) + i];
					sampleFor++;
				}
				if(down_half_for){  //Accumulate half pel
					ycbcr += prevFrame[prevIdx + 3*(j*hSizeMB + k + 1) + i];
					sampleFor++;
				}
				if(right_half_for && down_half_for){  //Accumulate half pel
					ycbcr += prevFrame[prevIdx + 3*((j+1)*hSizeMB + k + 1) + i];
					sampleFor++;
				}
			}
			ycbcrb = 0;
			if(backward) { //Backwards vector exists
				ycbcrb += backFrame[backIdx + 3*(j*hSizeMB + k) + i];
				sampleBack++;
				if(right_half_back){ //Accumulate half pel
					ycbcrb +=backFrame[backIdx + 3*(j*hSizeMB + k + 1) + i];
					sampleBack++;
				}
				if(down_half_back){ //Accumulate half pel
					ycbcrb += backFrame[backIdx + 3*(j*hSizeMB + k + 1) + i];
					sampleBack++;
				}
				if(right_half_back && down_half_back){ //Accumulate half pel
					ycbcrb += backFrame[backIdx + 3*((j+1)*hSizeMB + k + 1) + i];
					sampleBack++;
				}
			}
			if(!sampleBack) // Only sample forward
				currFrame[3*(j*hSizeMB + k) + i] = ycbcr / sampleFor;
			else if(!sampleFor) // Only sample backwards
				currFrame[3*(j*hSizeMB + k) + i] = ycbcrb / sampleBack;
			else // Sample both ways.
				currFrame[3*(j*hSizeMB + k) + i] = (ycbcrb / sampleBack + ycbcr / sampleFor)>>1;
		}
	}
}
