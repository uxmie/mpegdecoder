CXX = g++
CXXFLAGS = -O2 -Wall -Wextra -std=c++11 -pthread
SRC = src
RM = rm -f
LIBS = `pkg-config --libs opencv`
.PHONY: clean all
all:
	$(CXX) $(CXXFLAGS) huffman.cpp idct.cpp bitstream.cpp mpeg.cpp vlclookup.cpp main.cpp play.cpp -o mpegDecoder $(LIBS)
clean:
	-$(RM) mpegDecoder *.o
