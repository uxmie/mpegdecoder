#ifndef MPEG_BITSTREAM_H
#define MPEG_BITSTREAM_H

#include <stdio.h>
#include "mpegMacros.h"

/* The bit stream, used mainly for reading the input file. */
class bitStream{
public:
/* Constructor and destructor*/
	bitStream() {}
	bitStream(const char *fileName);
	~bitStream();
	void open(const char *fileName);
	
/* Reading */
	uint32_t readNbits(int nbits, bool pop);//Return number of bits read.
	inline uint32_t readNbits(int nbits){return readNbits(nbits, true);};
	void align();
	void dump();

private:
	const static unsigned int streamSize = 2048;
	FILE *fp;
	uint32_t byteOffset;
	uint32_t byteOffsetInBuffer;
	uint8_t bitOffset;
	uint8_t *buffer;
};

#endif 
