#ifndef __MPEG_HUFFMAN_H
#define __MPEG_HUFFMAN_H

#include <vector>
#include "bitstream.h"
#include "mpegMacros.h"
#define HUFFMAXLENGTH 16
struct huffmanEntry{
	huffmanEntry() {
		hasLeftChild = 0;
		rightOffset = 0;
	}
	bool hasLeftChild;
	short int data;
	int rightOffset;
	uint8_t padding[1];
};

struct lookupTableEntry {
	short int data;
	int length;
	uint8_t padding[2];
};

class huffmanTable {
public:
	huffmanTable();
	//uint8_t *createTable(char *stream);
	void pushSymbol(int length, int symbol, short int value);
	short int query(bitStream &stream);
	void dump();
	void dumpRecursive(int length, int value, int offset);
private:
	std::vector<huffmanEntry> tree;
	lookupTableEntry table[256];
};

/* Convert char to int */
inline int charToInt(char input) {
	return (int)input & ((1 << 8) - 1);
}
#endif
