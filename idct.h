#ifndef __JPEG_IDCT_H
#define __JPEG_IDCT_H

#include <stdint.h>
#include <math.h>

class IDCTsolver{
public:
	// Constructor: Save constants.
	IDCTsolver() {
		pi = 3.1415926535897932;
		sqrt2 = 1.4142135623730950488016887;

		double angle = 3.0*pi / 8.0;
		c[0] = cos(angle)*sqrt2; c[1] = sin(angle)*sqrt2;
		angle = pi / 16.0;
		c[2] = cos(angle); c[3] = sin(angle);
		angle *= 3.0;
		c[4] = cos(angle); c[5] = sin(angle);

	};
	/* Sove 1d and 2d IDCTs */
	void solve1D(double *input, double *output, bool skipEight);
	void solve2D(int *input, int *output);
private:
	double c[6], pi, sqrt2;
	double bOdd[8], bEven[8];
};


#endif
