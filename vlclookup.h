#ifndef MPEG_TABLES_H
#define MPEG_TABLES_H

#include "huffman.h"

/* Huffman tables */
struct Tables {
	huffmanTable mbAddrInc;
	huffmanTable mbTypeI;
	huffmanTable mbTypeP;
	huffmanTable mbTypeB;
	huffmanTable mbTypeDC;
	huffmanTable mbPattern;
	huffmanTable motionVector;
	huffmanTable dctSizeLum;
	huffmanTable dctSizeChr;
	huffmanTable runLength;

//Constructor (i.e. fill the tables)
	Tables();
};

#endif
