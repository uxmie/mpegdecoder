# ITCT Final project: MPEG-1 Decoder
By B02902104 陳以律

## Features
This project decodes an MPEG-1 sequence and plays it via OpenCV.

The thing I have implemented include:

* Fast IDCT and Huffman tables
* Multithreading: for simultaeneous decoding and playback. Only a fixed number of frames are decoded and put into a queue at once to prevent memory leaks.
* Real-time playback: Tested on a Linux Machine with Intel i5 processor.

## Compilation
You must have OpenCV2 to compile and run the program.

To compile it, simply run `make` to obtain the executable `mpegDecoder`.
`make clean` removes all executables.

## Usage
Run `./mpegDecoder filename` to decode and play the MPEG-1 video. Additional information will be printed on the terminal.
