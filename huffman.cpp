#include "huffman.h"
#include <stdio.h>
#include <stdlib.h>

/* Constructor */
huffmanTable::huffmanTable(){
	tree.push_back(huffmanEntry());
	for(int i = 0; i < 256; i++){
		table[i].data = 0;
		table[i].length = 0;
	}
}

/* Push a symbol into the tree. If shorter than 8 bits, create a lookup table.
 * Due to the way the table is built, all symbols must be pushed in pre-order
 * This makes the Huffman table unusable for general purposes, but faster in
 * our application.
 */
void huffmanTable::pushSymbol(int length, int symbol, short int value) {
	huffmanEntry temp;
	int currentOffset = 0;
	// Scan each bit
	for(int i = 1<<(length - 1); i; i >>= 1) {
		if(!(i & symbol)) { //Going right, jump to offset.
			if(!tree[currentOffset].rightOffset){ //Create entry.
				tree.push_back(huffmanEntry());
				tree[currentOffset].rightOffset = tree.size() - 1;
			}
			currentOffset = tree[currentOffset].rightOffset;
		} else { //Going left.
			if(!tree[currentOffset].hasLeftChild){ //Create entry.
				tree.push_back(huffmanEntry());
				tree[currentOffset].hasLeftChild = true;
			}
			currentOffset++;
		}
	}
	// Create entry in lookup table if less than 8 bits.
	if(length <= 8) {
		int maxAug = symbol << (8 - length) | ((1 << (8 - length)) - 1);
		//Fill in remaining bits.
		for(int i = symbol << (8 - length); i <= maxAug; i++) {
			table[i].data = value;
			table[i].length = length;
		}
	}
	//Debug
	if(tree[currentOffset].hasLeftChild || tree[currentOffset].rightOffset){
		fprintf(stderr, "Pushing: %d %x %d Not leaf node!!\n", length, symbol, value);
		exit(1);
	}
	tree[currentOffset].data = value;
	return;
}

/* Query the bit stream for next huffman table entry and decode it. */
short int huffmanTable::query(bitStream &stream) {
	uint8_t next8bits = stream.readNbits(8, false);
	//Try to find in lookup table
	if(table[next8bits].length){
		stream.readNbits(table[next8bits].length);
		return table[next8bits].data;
	}

	int currentOffset = 0, iterations = 0;
	//...or else find in the tree.
	while(iterations <= HUFFMAXLENGTH
	 &&(tree[currentOffset].hasLeftChild || tree[currentOffset].rightOffset)) {
		// If the next bit is 1, go left, or else go right.
		if(!stream.readNbits(1)){
			currentOffset = tree[currentOffset].rightOffset;
		} else {
			currentOffset++;
		}
	}
	// Value found.
	if(iterations <= HUFFMAXLENGTH){
		return tree[currentOffset].data;
	}
	fprintf(stderr, "Value not found! Exiting...\n");
	exit(1);
	return 0;
}

/* Recursively dump information in the Huffman tree.
 * Used only by huffmanTable::dump()
 */
void huffmanTable::dumpRecursive(int length, int value, int offset) {
	// If the node is a leaf, dump its info.
	if(!(tree[offset].hasLeftChild || tree[offset].rightOffset)) {
		fprintf(stderr, "Entry: ");
		// Dump binary format.
		for(int i = (1<<(length - 1)); i; i >>= 1) {
			fprintf(stderr, "%d", (value & i) <= 0);
		}
		fprintf(stderr, "\t\tData: 0x%x\n", tree[offset].data);
		return;
	}
	// ...or else dump left child.
	if(tree[offset].hasLeftChild)
		dumpRecursive(length + 1, value << 1, offset + 1);
	// ...and right child.
	if(tree[offset].rightOffset)
		dumpRecursive(length + 1, (value << 1) + 1, tree[offset].rightOffset);
}

/* Generate debug dump. Not used in the actual program */
void huffmanTable::dump() {
	dumpRecursive(0, 0, 0);
	fprintf(stderr, "Lookup Table:\n");
	for(int i = 0; i < 256; i++){ //Dump lookup table
		fprintf(stderr, "%2x : %2x, %d\n", i, table[i].data, table[i].length);
	}
}
